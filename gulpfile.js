var gulp = require("gulp");
var browserify = require("browserify");
var source = require('vinyl-source-stream');
var tsify = require("tsify");
var newer = require('gulp-newer');
var watchify = require("watchify");
var gutil = require("gulp-util");
var browserSync = require('browser-sync');
var reload = browserSync.reload;

var paths = {
    pages: ['src/*.html'],
    ts: ['src/**/*.ts'],
	css: ['src/css/**/*.css'],
	assets: ['src/assets/**'],
    vendor: ['src/vendor/**']
};

var watchedBrowserify = watchify(browserify({
    basedir: '.',
    debug: true,
    entries: ['src/main.ts'],
    cache: {},
    packageCache: {}
}).plugin(tsify));

gulp.task("copy-html", function () {
    return gulp.src(paths.pages)
		.pipe(newer("dist"))
        .pipe(gulp.dest("dist"));
});

gulp.task("copy-css", function () {
    return gulp.src(paths.css)
		.pipe(newer("dist/css"))
        .pipe(gulp.dest("dist/css"));
});

gulp.task("copy-assets", function () {
    return gulp.src(paths.assets)
		.pipe(newer("dist/assets"))
        .pipe(gulp.dest("dist/assets"));
});

gulp.task("copy-vendor", function () {
    return gulp.src(paths.vendor)
		.pipe(newer("dist/vendor"))
        .pipe(gulp.dest("dist/vendor"));
});

gulp.task("copy-html", function () {
    return gulp.src(paths.pages)
		.pipe(newer("dist"))
        .pipe(gulp.dest("dist"));
});

gulp.task('serve', function() {
  browserSync({
    server: {
      baseDir: 'dist'
    }
  });

  gulp.watch(['dist/*.html', 'dist/css/**/*.css', 'dist/assets/**', '**/*.js'], {cwd: 'dist'}, reload);
  gulp.watch('src/**/*.ts', ["ts-build"]);
  gulp.watch('src/*.html', ["copy-html"]);
  gulp.watch('src/css/**/*.css', ["copy-css"]);
  gulp.watch('src/assets/**', ["copy-assets"]);
  gulp.watch('src/vendor/**', ["copy-vendor"]);
});

gulp.task('ts-build', function() {
	browserify({debug: true})
    .add('src/main.ts')
    .plugin(tsify)
    .bundle()
    .on('error', function (error) { console.error(error.toString()); })
    .pipe(source('bundle.js'))
    .pipe(gulp.dest("dist"));
});

function bundle() {
	return watchedBrowserify
        .bundle()
        .pipe(source('bundle.js'))
        .pipe(gulp.dest("dist"));
};

gulp.task("default", ["copy-html", "copy-css", "copy-assets", "copy-vendor", "ts-build"]);
watchedBrowserify.on("update", bundle);
watchedBrowserify.on("log", gutil.log);