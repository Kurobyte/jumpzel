import Game from './ts/Game';

let game = new Game();
let c_opacity = 0;
let lvls = JSON.parse(localStorage.getItem('levels'));

function render() {
    let req = requestAnimationFrame(render);
    game.render(req);
}

/**
 * Funció que fa apareixre o desapareixre el canvas amb un efecte fade
 * @param {boolean} hide Toggle per determinar si el volem mostrar o amagar
 */
function toggleCanvas(hide: boolean) {
    let op = setInterval(() => {
        c_opacity += (hide ? -0.10 : 0.05);
        
        $('canvas').css('opacity', c_opacity);
        if (hide) {
            if (c_opacity <= 0) {
                clearInterval(op);
                c_opacity = 0;
            }
        } else {
            if (c_opacity >= 1) {
                clearInterval(op);
                c_opacity = 1;
            }
        }
            
    }, 50);
}

/**
 * Funció que mostra la pantalla del joc
 * 
 */
function showLvlScreen(): void {
    //render ON
    render();
    $('canvas').css('opacity', '0');
    $('#hud').css('opacity', '1'); //Mostrem el HUD
    toggleCanvas(false); //Activem el canvas
}

/**
 * Funcio cridada per event click que permet retornar a la pantalla de seleccio de nivells
 * @param {JQueryEventObject} e 
 */
function comeBackToLevelScreen(e: JQueryEventObject) {
    if (e.target.id == "endClear-levels") { //Determinem desde on ens fan click
        $('#gameClear-menu').css('opacity', '0'); //Amaguem la UI pertinent
        setTimeout(() => { $('#gameClear-menu').css('visibility', 'hidden'); }, 1000);
    } else if (e.target.id == "endOver-levels") {
        $('#gameOver-menu').css('opacity', '0');
        setTimeout(() => { $('#gameOver-menu').css('visibility', 'hidden'); }, 1000);
    } else {
        $('#pause-menu').css('opacity', '0');
        $('#hud').css('opacity', '0');
        toggleCanvas(true);
        setTimeout(() => { $('#pause-menu').css('visibility', 'hidden'); }, 1000);
    }

    $('#levels-menu').css('visibility', 'visible');
    $('#levels-menu').css('opacity', '1');
}

/**
 * Funció que calcula la puntuació i la transforma en un % per mostrar a la UI
 * @param {number} p Punts del jugador
 */
function setPuntuacio(p: number): void {
    let punt = Math.round(p);
    let percent = 0;

    switch (punt) {
        case 1:
        percent = 20;
        break;

        case 2:
        percent = 33;
        break;

        case 3:
        percent = 54;
        break;

        case 4:
        percent = 71;
        break;

        case 5:
        percent = 100;
        break;
    }

    $("#punts-esq").children('.progress').css('width', percent+"%");
    $("#punts-dre").children('.progress').css('width', percent+"%");
    $("#punts-text").html(punt+'/5');
}

/**
 * Funcio que mostra el menú final a cada partida
 * @param {boolean} over Boolea que determina si el jugador ha guanyat o perdut
 * @param {number} p Punts del jugador
 * @param {number} l Nivell que ha completat
 */
function showEndMenu(over: boolean, p: number, l: number): void {
    $('#hud').css('opacity', '0');
    toggleCanvas(true);

    if (!over) {
        setPuntuacio(p);
        $('#gameClear-menu').css('visibility', 'visible');
        $('#gameClear-menu').css('opacity', '1');
        let np = Math.round(p);
        let codedUri = encodeURIComponent('I\'ve got '+ np +' of 5 points on #Jumpzel lvl '+ (l+1) +' http://jumpzel.gitlab.io');
        $('#tweetPoints').attr('href', 'http://twitter.com/home/?status=' + codedUri);

        let divL = $('.level').toArray()[l];
        setLvlDone(divL);

        if (l != 15) {
            $('#end-next').css('visibility', 'visible');
            $('#end-next').off('click');
            $('#end-next').click((e) => {
                $('#gameClear-menu').css('opacity', '0');
                setTimeout(() => { $('#gameClear-menu').css('visibility', 'hidden'); }, 1000);
                let nl = l + 1;
                game.loadLevel("lvl"+nl, showEndMenu, showPauseMenu, showLvlScreen);
                setTimeout(() => { setPuntuacio(0); }, 500);
            });

            let elem = $('.level').toArray()[l+1]; //Activa el proxim nivell al menu
            elem.className = elem.className.replace('lvl-disabled', '');
            lvls['lvl'+(l+1)].active = true; //Activem el proxim nivell al model de dades
            
        } else {
            $('#end-next').css('visibility', 'hidden');
        }

        lvls['lvl'+l].completed = true;
        lvls['lvl'+l].points = p;
        localStorage.setItem('levels', JSON.stringify(lvls));

    } else {
        $('#gameOver-menu').css('visibility', 'visible');
        $('#gameOver-menu').css('opacity', '1');

        $('#end-retry').off('click');
        $('#end-retry').click((e) => {
            $('#gameOver-menu').css('opacity', '0');
            setTimeout(() => { $('#gameOver-menu').css('visibility', 'hidden'); }, 1000);
            game.loadLevel("lvl"+l, showEndMenu, showPauseMenu, showLvlScreen);
        });
    }

}

/**
 * Funcio que mostra la finestra de pausa
 */
function showPauseMenu(): void {
    $('#pause-menu').css('visibility', 'visible');
    $('#pause-menu').css('opacity', '1');

    game.clock.stop();
}

/**
 * Funcio assigna efectes a la UI si el nivell ha estat completat
 * @param {HTMLElement} elem Element de la UI
 */
function setLvlDone(elem: HTMLElement): void {
    let n = <any>elem.childNodes[3];
    if (n.className.lastIndexOf('done') == -1) {
        n.className = n.className + ' border-done';
        let n2 = <any>elem.childNodes[3].childNodes[1];
        n2.className = n2.className + ' done';
    }
}

/**
 * Funcio que activa la carrega del nivell sel·leccionat
 * @param {string} name Nom del nivell
 */
function loadLevel(name: string) {
    setPuntuacio(0);
    game.loadLevel(name, showEndMenu, showPauseMenu, showLvlScreen);
    $('#levels-menu').css('opacity', '0');
    setTimeout(() => { $('#levels-menu').css('visibility', 'hidden'); }, 1000);
}


////////Events


$('#play-button').click((e) => {
    $('#main-menu').fadeOut(1000, () => { 
        $('#main-menu').css('display', 'none');
        $('#levels-menu').css('visibility', 'visible');
        $('#levels-menu').css('opacity', '1');
    });
});

$('#info-button').click((e) => {
    $('#info-menu').css('visibility', 'visible');
    $('#info-menu').css('opacity', '1');
});

$('#back-button').click((e) => {
    $('#info-menu').css('opacity', '0');
    setTimeout(() => { $('#info-menu').css('visibility', 'hidden'); }, 1000);
});

$('.level').toArray().forEach((elem) => { //Assignem el event click als nivells
    elem.addEventListener("click", (e) => { loadLevel(elem.dataset['level']); });
    if (lvls[elem.dataset['level']].completed) {
        setLvlDone(elem);
    }

    if (lvls[elem.dataset['level']].active)
        elem.className = elem.className.replace('lvl-disabled', '');
});

$('#endClear-levels').click(comeBackToLevelScreen);
$('#endOver-levels').click(comeBackToLevelScreen);
$('#pause-levels').click(comeBackToLevelScreen);
$('#pause-continue').click((e) => {
    $('#pause-menu').css('opacity', '0');
    setTimeout(() => { 
        $('#pause-menu').css('visibility', 'hidden'); 
        render();
        game.clock.start();
    }, 1000);
});
