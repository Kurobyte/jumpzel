
export module Keys { 
    export var KeyCodes = {
        LEFT: 37,
        UP: 38,
        RIGHT: 39,
        DOWN: 40,
        SHIFT: 16,
        SPACE: 32
    };

    class InnerEvents {
        _pressed: any = {};

        constructor() {}
        
        isDown(keyCode: any) {
            return this._pressed[keyCode];
        }

        onKeyDown(event: JQueryKeyEventObject) {
            this._pressed[event.keyCode] = true;
        }

        onKeyUp(event: JQueryKeyEventObject) {
            delete this._pressed[event.keyCode];
        }
    }


    export var Events: InnerEvents;
    Events = new InnerEvents();

}

export default Keys;