
/**
 * Enumerador per determinar el tipus de moviment del personatge
 * 
 * @enum {number}
 */
enum AniTypes {
    IDLE,
    JUMP,
    LONG_JUMP,
    DEATH
}

/**
 * Enumerador per determinar la posicio del personatge
 * 
 * @enum {number}
 */
enum AniDirection {
    UP,
    DOWN,
    LEFT,
    RIGHT
}

/**
 * Classe que conte les funcions per animar el perssonatge
 * 
 * @export
 * @class AniMaker
 */
export default class AniMaker {
    block_dist: number = 0;
    char: THREE.Object3D;

    PLAYING: boolean = false;
    status: AniTypes = AniTypes.IDLE;
    death: boolean;
    axis: string;
    cb: () => void;

    duration: number = 0;
    dist: number = 1;
    time: number = 0;

    //Posicions originals del personatge
    ori_xyz: any = {x: 0, z: 0, y: 0};

    //Velocitats i acceleració
    vxz0: number = 2;
    vy0: number = 24;
    v0: number = 1;
    ay: number = 0;


    static TYPES = AniTypes; //Static class enum
    static DIRECTIONS = AniDirection;

    /**
     * Inicialitza una instancia de la classe
     * @param {THREE.Mesh} c Mesh en el que volem animar la posicio
     * @param {number} dist Distancia entre plataformes
     * 
     * @memberOf AniMaker
     */
    constructor(c: THREE.Object3D, dist: number) {
        this.char = c;
        this.block_dist = dist;
    }

    /**
     * Funcio que reprodueix una animacio
     * 
     * @param {AniTypes} anim Tipus de animacio a reproduir
     * @param {AniDirection} dire Direccio en la que reproduira la animaciò
     * @param {boolean} death El mesh morira al executar la animacio
     * @param {() => void} callback Funcio que s'executará al acabar la animacio
     * 
     * @memberOf AniMaker
     */
    play(anim: AniTypes, dire: AniDirection, death: boolean, callback: () => void) {
        this.PLAYING = true;
        this.time = 0;
        this.death = death;
        this.cb = callback;
        this.status = anim;

        this.ori_xyz.x = this.char.position.x;
        this.ori_xyz.z = this.char.position.z;
        this.ori_xyz.y = this.char.position.y;

        switch(anim) {
            case AniTypes.JUMP:
                this.dist = 1;
                this.duration = 0.5; //seconds
                break;

            case AniTypes.LONG_JUMP:
                this.dist = 2;
                this.duration = 1; //seconds
                break;
            case AniTypes.DEATH:
                this.duration = 2;
                break;
        }

        switch(dire) { //Determinem la direccio a moure
            case AniDirection.UP:
            this.v0 = -1;
            this.vxz0 = ((this.dist * this.block_dist) / this.duration) * this.v0;
            this.axis = 'z';
            break;

            case AniDirection.DOWN:
            this.v0 = 1;
            this.vxz0 = ((this.dist * this.block_dist) / this.duration) * this.v0;
            this.axis = 'z';
            break;

            case AniDirection.LEFT:
            this.v0 = -1;
            this.vxz0 = ((this.dist * this.block_dist) / this.duration) * this.v0;
            this.axis = 'x';
            break;

            case AniDirection.RIGHT:
            this.v0 = 1;
            this.vxz0 = ((this.dist * this.block_dist) / this.duration) * this.v0;
            this.axis = 'x';
            break;
        }

        this.ay = (( this.vy0 * 2) / this.duration); //Calculem la acceleracio de la Y

        if (death)
            this.duration = 2;
    }

    /**
     * Funcio que processa el increment de la animacio utilizant el DELTA
     * 
     * @param {number} delta 
     * 
     * @memberOf AniMaker
     */
    update(delta: number) {
        if ((this.status == AniTypes.JUMP || this.status == AniTypes.LONG_JUMP) && !this.death) {
            this.jump(delta);
        } else if (this.status == AniTypes.DEATH) {
            this.liberaMeFromHell(delta);
        } else {
            this.jump2Death(delta);
        }
    }

    /**
     * Funcio que genera el salt normal entre plataformes
     * @param delta increment del temps
     */
    private jump(delta: number): void {
        let xz = (this.ori_xyz[this.axis] + (this.vxz0 *  (this.time + delta)));
        let y = (this.ori_xyz.y + (this.vy0 * (this.time + delta)) - ( 0.5 * this.ay * (this.time + delta) * (this.time + delta))); //Parabola

        if (this.axis == 'z')
            this.char.position.z = xz;
        else
            this.char.position.x = xz;
        this.char.position.y = y;

        this.time += delta;

        if (this.time >= this.duration) {
            xz = (this.ori_xyz[this.axis] + (this.dist * this.block_dist) * this.v0); //Fixem la posicio final per a que no sobresurti de la plataforma
            if (this.axis == 'z')
                this.char.position.z = xz;
            else
                this.char.position.x = xz;
            
            this.char.position.y = this.ori_xyz.y;

            this.PLAYING = false;
            this.status = AniTypes.IDLE;
            if (this.cb)
                this.cb();
        }
    }

    /**
     * Funcio que anima un salt del personatge al abisme
     * @param delta Increment del temps
     */
    private jump2Death(delta: number): void {
        let xz = (this.ori_xyz[this.axis] + (this.vxz0 *  (this.time + delta)));
        let y = (this.ori_xyz.y + (this.vy0 * (this.time + delta)) - ( 0.5 * this.ay * (this.time + delta) * (this.time + delta))); //Parabola

        if (this.axis == 'z')
            this.char.position.z = xz;
        else
            this.char.position.x = xz;
        this.char.position.y = y;

        this.time += delta;

        if (this.time >= this.duration) {
            let real: THREE.Mesh = <THREE.Mesh>this.char.children[0];
            real.material.materials[0].opacity = 0; //Ignorar error
            xz = this.ori_xyz[this.axis];
            if (this.axis == 'z')
                this.char.position.z = xz;
            else
                this.char.position.x = xz;
            
            this.char.position.y = this.ori_xyz.y;

            this.play(AniTypes.DEATH, AniDirection.UP, false, this.cb);
        }
    }

    /**
     * Funcio que fa una animacio per reviure el personatge.
     * @param delta Increment de temps
     */
    liberaMeFromHell(delta: number) {
        let op = (1 + Math.sin(this.time + this.time * 0.75 + delta)) % 1;
        //console.log(op, this.time + delta);
        this.time += delta;
        let real: THREE.Mesh = <THREE.Mesh>this.char.children[0];
        real.material.materials[0].opacity = op; //Ignorar error

        if (this.time >= this.duration) {
            real.material.materials[0].opacity = 1; //Ignorar error

            this.PLAYING = false;
            this.status = AniTypes.IDLE;
            if (this.cb)
                this.cb();
        }
    }
}

