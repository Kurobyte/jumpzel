/**
 * Clase que conte la logica del nivell
 * 
 * @export
 * @class Level
 */
export default class Level {
    platforms: [boolean[]];
    startPos: LvlPosition;
    actualPos: LvlPosition;
    strColor: string;
    normalMaterials: THREE.MeshBasicMaterial[];
    steppedMaterials: THREE.MeshBasicMaterial[];
    defaultColor: string = "#F6F6F6";
    lives: number;
    jumps: number;
    nSteps: number;
    maxJumps: number;
    maxTime: number;
    maxLives: number;

    meshPlatforms: [THREE.Mesh[]];
    steppedPlatforms: [boolean[]];
    objPlatforms: THREE.Mesh[];

    constructor(platforms: [boolean[]], startPos: LvlPosition, steppedColor: string, lives: number = 3, jumps: number, time: number) {
        this.meshPlatforms = [[]];
        this.objPlatforms = [];
        this.platforms = platforms;
        this.startPos = startPos;
        this.actualPos = { x: startPos.x, z: startPos.z };
        this.strColor = steppedColor;
        this.lives = lives;
        this.maxLives = lives;
        this.maxJumps = jumps;
        this.maxTime = time;

        this.jumps = -1;
        this.nSteps = 0;
    }

    /**
     * Funcio que comproba si la posicio pasada per parametre es una posicio valida al nivell
     * 
     * @param {number} x Posicio X de la plataforma
     * @param {number} z Posicio Z de la plataforma
     * @returns {boolean} Retornará true sempre que sigui una posicio valida
     * 
     * @memberOf Level
     */
    posicioValida(x: number, z: number): boolean {
        return (x >= 0 && x < this.platforms.length) && (z >= 0 && z < this.platforms[x].length) && (this.meshPlatforms[x][z] != undefined);
    }

    /**
     * Funcio que fa un pas a una plataforma
     * 
     * @param {number} x Posicio X de la plataforma
     * @param {number} z Posicio Z de la plataforma
     * @returns {boolean} Retornará true sempre que sigui una posicio valida
     * 
     * @memberOf Level
     */
    step(x: number, z: number): boolean {
        let result = false;
        if (this.posicioValida(x, z)) {
            this.actualPos.x = x; this.actualPos.z = z;
            this.colorPosition(x, z);
            this.steppedPlatforms[x][z] = !this.steppedPlatforms[x][z];
            result = true;
            this.nSteps += this.steppedPlatforms[x][z] ? 1 : -1;
            this.jumps += 1;
        }

        return result;
    }

    /**
     * Funcio que incrementa en 1 el nº de salts si el jugador salta al vuit.
     * @memberof Level
     */
    deathStep(): void {
        this.jumps += 1;
    }

    /**
     * Funcio que canvia el color de la plataforma en la cual el jugador ha saltat
     * 
     * @param {number} x Posicio X de la plataforma
     * @param {number} z Posicio Z de la plataforma
     * 
     * @memberOf Level
     */
    colorPosition(x: number, z: number): void {
        let mat = !this.steppedPlatforms[x][z] ? this.steppedMaterials : this.normalMaterials;
        this.meshPlatforms[x][z].material = new THREE.MeshFaceMaterial( mat );
    }

    calcPuntuacio(time: number): number {
        let l = (this.lives / this.maxLives) * 1.66;
        let t = (this.maxTime / time) > 1 ? 1.66 : (this.maxTime / time) * 1.66;
        let j = (this.maxJumps / this.jumps) * 1.66;
        
        return (l + t + j);
    }
}

export interface LvlPosition { x: number, z: number }