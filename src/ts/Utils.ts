import Level from './Level'

/**
 * Modul que conte utilitats generals per al joc
 */
export module Utils {
    /**
     * Funcio que carrega de manera asincrona models 3D en format JSON
     * @param files llista de 
     * @param callback Funcio que será cridada un cop tots els models s'hagin acabat de carregar
     */
    export function jsonModelLoader(files: ModelFiles[], callback: (MeshList: ModelFiles[]) => any) {
        let loader = new THREE.JSONLoader();
        let j = 0;
        let objects: any = {}

        for (let i = 0; i < files.length; i++) {
            let file = files[i];

            loader.load( file.fileName, ( geometry: THREE.Geometry, materials: THREE.Material[] ) => {
                objects[file.name] = { name: file.name, geometry: geometry, materials: materials };
                j++;
                if (j == files.length) {
                    callback( objects );
                }
            });
        }

    }

    /**
     * Funcio que carrega de manera asincrona models 3D en format JSON
     * @param files llista de 
     * @param callback Funcio que será cridada un cop tots els models s'hagin acabat de carregar
     */
    export function colladaModelLoader(files: ModelFiles[], callback: (MeshList: ModelFiles[]) => any) {
        let loader = new THREE.ColladaLoader();
        loader.options.convertUpAxis = true; //Ignorar advertencia, la sintaxis es correcte - https://threejs.org/docs/#Examples/Loaders/ColladaLoader
        let i = 0;
        let objects: any = {}

        for (let i = 0; i < files.length; i++) {
            let file = files[i];

            loader.load( file.fileName, ( collada: THREE.ColladaModel ) => {
                console.log(collada);
                objects[file.name] = { name: file.name, collada: collada };
                i++;
                if (i == files.length) {
                    callback( objects );
                }
            });
        }

    }

    /**
     * Funcio que carrega les dades de un nivell en format JSON
     * @param lvlName Nom del fitxer que conte el nivell a carregar. Ex: lvl0.json
     * @param callback Funcio que s'executarà un cop carregat el nivell
     */
    export function levelLoader(lvlName: string, callback:(lvl: Level, cb: () => void) => any, cb: () => void): void {
        $.getJSON('./assets/levels/'+lvlName, (data) => {
            callback(new Level(data.platforms, data.startPos, data.steppedColor, data.lives, data.jumps, data.time), cb);
        })
    }

    /**
     * Funcio que converteix graus a radians.
     * @param {number} dg Graus
     */
    export function deg2rad(dg: number) {
        return dg * (Math.PI / 180);
    }

    /**
     * Funcio que converteix segons a string en format 'mm:ss'
     * 
     * @export
     * @param {number} seg Segons
     * @returns {string}
     */
    export function toMinutes(seg: number): string {
        let min = Math.floor(seg / 60);
        let secon = Math.floor(seg % 60);

        return (min < 10 ? "0"+min : ""+min) + ":" + (secon < 10 ? "0"+secon : ""+secon);
    }

    /**
     * Funcio que genera una textura solida amb el color que pasem per parametre
     * @param color Color que tindra la textura
     * @returns {HTMLCanvasElement} 
     */
    export function genSolidTexture(color: string = '#F6F6F6'): HTMLCanvasElement {
        let size = 512;

        // create canvas
        let canvas = document.createElement( 'canvas' );
        canvas.width = size;
        canvas.height = size;

        // get context
        let context = canvas.getContext( '2d' );

        // emplena
        context.rect( 0, 0, size, size );
        context.fillStyle = color;
        context.fill();

        return canvas;
    }

    /**
     * Funcio que genera una textura amb degradat transparent
     * @export
     * @param {string} [color='#F6F6F6'] Color que tindra la textura
     * @param {number} perc Percentatge de superficie coverta per el degradat 0.1 -> 1
     * @returns {HTMLCanvasElement} 
     */
    export function genGradTexture(color: string = '#F6F6F6', perc: number): HTMLCanvasElement {
        let size = 512;

        // create canvas
        let canvas = document.createElement( 'canvas' );
        canvas.width = size;
        canvas.height = size;

        // get context
        let context = canvas.getContext( '2d' );

        // degradat
        context.rect( 0, 0, size, size );
        let gradient = context.createLinearGradient( 0, 0, 0, size );
        gradient.addColorStop(0, color); //Definim el primer color
        gradient.addColorStop(perc, 'transparent'); // definim el segon color(transparent)
        context.fillStyle = gradient;
        context.fill();

        return canvas;
    }

    export function genPlatformMaterials(color: string, perc: number): THREE.MeshBasicMaterial[] {
        let texture = new THREE.Texture( this.genSolidTexture(color) );
        texture.needsUpdate = true; // important!
        let texture2 = new THREE.Texture( this.genGradTexture(color, perc) );
        texture2.needsUpdate = true; // important!

        let material = new THREE.MeshBasicMaterial( { map: texture } );
	    let material2 = new THREE.MeshBasicMaterial( { map: texture2, transparent: true } );

        return [material2, material2, material, material, material2, material2];
    }
}

/**
 * Interficie per enpaquetar la llista de models
 */
export interface ModelFiles {
    name: string,
    fileName?: string, //Parametre opcional
    geometry?: THREE.Geometry, //Parametre opcional
    materials?: THREE.Material[] //Parametre opcional
    collada?: THREE.ColladaModel
}

export interface Actions {
    [key: string]: THREE.AnimationAction;
}

export interface Sounds {
    [key: string]: HTMLAudioElement;
}

export default Utils;