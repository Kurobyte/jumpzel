import { LvlPosition } from './Level';
import Level from './Level';
import AniMaker from './AniMaker';
import { Utils, ModelFiles, Actions, Sounds } from './Utils';
import { Keys } from './Keys';

export default class Game {
    renderer: THREE.WebGLRenderer;
    scene: THREE.Scene;
    camera: THREE.OrthographicCamera;
    clock: THREE.Clock;
    caster: THREE.Raycaster;
    actions: Actions = {};
    action: any = {};
    anim: AniMaker;

    renderFnc: any[] = [];
    reqID: number;
    clearCB: (over: boolean, p: number, l: number) => void;
    pauseCB: () => void;

    sceneObjects: any = {};
    objMaterials: ModelFiles[];
    sounds: Sounds = {};

    level: Level;
    levelID: number;
    char: THREE.Object3D;
    nPos: LvlPosition;
    colided: boolean;
    time: number;
    saveTime: number = 0;

    /**
     * Constructor que inicialitza la classe Game.
     * Conté tota la logica del gameloop
     */
    constructor() {
        this.renderer = new THREE.WebGLRenderer({ // Inicialitzem el renderer
            antialias: true,
            alpha: true
        });
	    this.renderer.setSize( window.innerWidth, window.innerHeight );
        this.caster = new THREE.Raycaster();

        $('body').append(this.renderer.domElement); //Afegim el element canvas al DOM
        $(window).resize((event) => {
            this.renderer.setSize( window.innerWidth, window.innerHeight )
            this.camera.updateProjectionMatrix()		
        });
        $(window).keydown((event) => { Keys.Events.onKeyDown(event); });
        $(window).keyup((event) => { Keys.Events.onKeyUp(event); });

        //Inicialitzem la escena
        this.scene	= new THREE.Scene();

        //Instanciacio de la camara
        var aspect = window.innerWidth / window.innerHeight; //Relacio de aspecte de la pantalla
        var d = 9.6; //Distancia
        this.camera = new THREE.OrthographicCamera( - d * aspect, d * aspect, d, - d, 1, 1000 );

        this.camera.position.set( 22, 7, 22.4 ); // Posicionem la camara
        this.camera.lookAt( new THREE.Vector3(0, -10, 0)); // Assignem el punt de visio de la camara
        //Canviem el angle de la camara
        this.camera.setRotationFromEuler(new THREE.Euler(-0.5235987755982988, 0.6981317007977318, 0.3665191429188092, 'XYZ'));

        //Carreguem el audio
        this.sounds['pause'] = new Audio("assets/audio/pause.mp3");
        this.sounds['land'] = new Audio("assets/audio/land.mp3");
        this.sounds['win'] = new Audio("assets/audio/win.mp3");
        this.sounds['loose'] = new Audio("assets/audio/loose.mp3");
        this.sounds['death'] = new Audio("assets/audio/ded.mp3");

        //Carreguem els models
        Utils.jsonModelLoader([
            { name: 'cubeTronic', fileName: './assets/models/box2.json'}
            ], this.getJsonObjects);

        
        //Inicialitzem el render
        //Afegim funcio al render per actualitzar la posicio del personatge
        this.renderFnc.push(() => { this.updateCharacter() });
        this.renderFnc.push(() => { this.collision() });
        this.renderFnc.push(() => { 
            var delta = this.clock.getDelta();
            
            THREE.AnimationHandler.update(delta);
            if (this.anim.PLAYING) {
                this.anim.update(delta);
            }
        });
        this.renderFnc.push(() => { this.updateUIClock() });
        this.renderFnc.push(() => { this.renderer.render(this.scene, this.camera); });
        this.renderFnc.push(() => { this.updateOverClear() });

        if (localStorage.getItem('levels') == undefined) {
            let lvList: any = {};
            for (let i = 0; i < 16; i++) {
                lvList['lvl'+i] = { active: false, completed: false, points: 0 }
            }
            lvList['lvl0'].active = true;

            localStorage.setItem('levels', JSON.stringify(lvList));
        }
    }

    /**
     * Funcio de render, simplement executa la logica del joc i renderitza
     * @param {number} req ID per poder parar el RequestAnimation en cas de que el jugaor guanyi o perdi
     * @memberof Game
     */
    render(req: number): void {
        this.reqID = req;
        this.renderFnc.forEach((fn) => { fn() });
    }

    /**
     * Funcio que activa carrega de un nivell desde la UI
     * 
     * @param {string} lvlName Nom del nivell
     * @param {(over: boolean, p: number, l: number) => void} clearGameCB Callback que s'executará un cop completat el nivell
     * @param {() => void} cb Callback que s'executará un cop carregat el nivell
     * @memberof Game
     */
    loadLevel(lvlName: string, clearGameCB: (over: boolean, p: number, l: number) => void, pauseGameCB: () => void, cb: () => void): void {
        this.clearScene();
        $('#hud-time-clock').html(Utils.toMinutes(0)); //Clear game clock
        this.colided = false;
        this.level = undefined;
        this.clearCB = clearGameCB;
        this.pauseCB = pauseGameCB;
        this.levelID = parseInt(lvlName.substr(3, lvlName.length));
        Utils.levelLoader(lvlName +'.json', this.getLvl, cb);
    }

    /**
     * Funcio que controla si hi ha una colisio entre el cub i les plataformes
     * @memberof Game
     */
    collision(): void {
        if (!this.colided) {
            this.caster.set(this.char.position, new THREE.Vector3(0, -1, 0));
            var collide = this.caster.intersectObjects(this.level.objPlatforms);

            if (collide.length > 0 && collide[0].distance <= 0.3) {                
                this.colided = true;
                this.level.step(this.nPos.x, this.nPos.z);
                this.sounds.land.play();
            }
        }
    }

    /**
     * Funcio que actualitza el Rellotge de la UI
     * @memberof Game
     */
    updateUIClock(): void {
        let t = Math.floor(this.clock.getElapsedTime());
        if (t > this.time) {
            this.time = t;
            
            $('#hud-time-clock').html(Utils.toMinutes(t));
        }
    }

    /**
     * Funcio que actualitza el comptador de salts de la UI
     * @memberof Game
     */
    updateUIJumps(): void {
        $('#hud-jump-count').html(this.level.jumps+"");
    }

    /**
     * Funcio que actualitza les vides de la UI
     * @memberof Game
     */
    updateUILives(): void {
        let liv = $('.lives');
        
        liv[this.level.lives].className += " nolife"
    }

    /**
     * Funcio que controlará si el jugador ha Guanyat o Perdut el nivell
     */
    updateOverClear(): void {
        if (this.level.lives > 0) {
            if (this.level.nSteps == this.level.objPlatforms.length) { //GAME CLEAR
                cancelAnimationFrame(this.reqID);
                this.updateUIClock();
                this.updateUIJumps();
                
                this.sounds.win.play();
                this.clearCB(false, this.level.calcPuntuacio(this.time), this.levelID);
            }
        } else { //GAME OVER
            cancelAnimationFrame(this.reqID);
            this.updateUIClock();
            this.updateUIJumps();
            this.sounds.loose.play();
            this.clearCB(true, 0, this.levelID);
        }
    }

    /**
     * Funcio que actualitza el perssonatge si aquest ha apretat una tecla de moviment
     * @memberof Game
     */
    updateCharacter(): void {

        if (!this.anim.PLAYING && this.colided && this.level.lives > 0) {
            let animType = AniMaker.TYPES.JUMP;
            let gap: number = 1;
            let aPos: LvlPosition = this.level.actualPos;
            let anim = this.actions.jump;

            if (Keys.Events.isDown(Keys.KeyCodes.SHIFT)) {
                animType = AniMaker.TYPES.LONG_JUMP;
                anim = this.actions.long_jump;
                gap = 2;
            }

            if (Keys.Events.isDown(Keys.KeyCodes.UP)) {
                if (this.level.posicioValida(aPos.x - gap, aPos.z)) {
                    this.nPos = {x: aPos.x - gap, z: aPos.z};
                    this.char.rotateY(Utils.deg2rad(90));
                    this.char.position.x -= 1; //Position fix
                    anim.play();
                    this.anim.play(animType, AniMaker.DIRECTIONS.UP, false, () => {
                        this.colided = false;
                        anim.stop();
                        this.char.rotateY(Utils.deg2rad(-90));
                        this.char.position.x += 1; //Position fix
                        this.actions.idle.play();
                     });
                } else {
                    this.level.deathStep();
                    this.sounds.death.play();
                    this.anim.play(animType, AniMaker.DIRECTIONS.UP, true, () => {
                        this.level.lives -= 1;
                        this.updateUILives();
                    });
                }

            } else if (Keys.Events.isDown(Keys.KeyCodes.DOWN)) {
                if (this.level.posicioValida(aPos.x + gap, aPos.z)) {
                    this.nPos = {x: aPos.x + gap, z: aPos.z};
                    this.char.rotateY(Utils.deg2rad(-90));
                    anim.play();
                    this.anim.play(animType, AniMaker.DIRECTIONS.DOWN, false, () => { 
                        this.colided = false;
                        anim.stop();
                        this.char.rotateY(Utils.deg2rad(90));
                        this.actions.idle.play();
                     });
                } else {
                    this.level.deathStep();
                    this.sounds.death.play();
                    this.anim.play(animType, AniMaker.DIRECTIONS.DOWN, true, () => {
                        this.level.lives -= 1;
                        this.updateUILives();
                    });
                }

            }  else if (Keys.Events.isDown(Keys.KeyCodes.LEFT)) {
                if (this.level.posicioValida(aPos.x, aPos.z - gap)) {
                    this.nPos = {x: aPos.x, z: aPos.z - gap};
                    this.char.rotateY(Utils.deg2rad(-180));
                    this.char.position.z += 1; //Position fix
                    anim.play();
                    this.anim.play(animType, AniMaker.DIRECTIONS.LEFT, false, () => { 
                        this.colided = false;
                        anim.stop();
                        this.char.rotateY(Utils.deg2rad(180));
                        this.char.position.z -= 1;
                        this.actions.idle.play();
                    });
                } else {
                    this.level.deathStep();
                    this.sounds.death.play();
                    this.anim.play(animType, AniMaker.DIRECTIONS.LEFT, true, () => { 
                        this.level.lives -= 1;
                        this.updateUILives();
                    });
                }

            } else if (Keys.Events.isDown(Keys.KeyCodes.RIGHT)) {
                if (this.level.posicioValida(aPos.x, aPos.z + gap)) {
                    this.nPos = {x: aPos.x, z: aPos.z + gap};
                    anim.play();
                    this.anim.play(animType, AniMaker.DIRECTIONS.RIGHT, false, () => { 
                        this.colided = false;
                        anim.stop();
                        this.actions.idle.play();
                    });
                } else {
                    this.level.deathStep();
                    this.sounds.death.play();
                    this.anim.play(animType, AniMaker.DIRECTIONS.RIGHT, true, () => {
                        this.level.lives -= 1;
                        this.updateUILives();
                    });
                }

            } else if (Keys.Events.isDown(Keys.KeyCodes.SPACE)) { //PAUSE GAME
                cancelAnimationFrame(this.reqID);
                this.saveTime = this.time;
                this.sounds.pause.play();
                this.pauseCB();
            }

            this.updateUIJumps();
        }
    }

    /**
     * Funcio de callback que s'executará un cop carregats els models
     * @memberof Game
     */
    public getJsonObjects = (obj: any): void => {
        if (this.objMaterials == undefined) {
            this.objMaterials = obj;
        } else {
            this.objMaterials = $.extend(this.objMaterials, obj);
        }

        let mesh: any = this.objMaterials;

        let chara = new THREE.Mesh( mesh['cubeTronic'].geometry, new THREE.MeshFaceMaterial( mesh['cubeTronic'].materials ) );
        chara.name = "cubeMaster";
        chara.material.materials[0].transparent = true;
        this.char = new THREE.Object3D();
        //this.char.castShadow = true;
        this.char.add(chara);
        this.char.scale.set(.7, .7, .7); //Escalem el contenidor Object3D

        //Inicialitzem el control d'animació del cub
        this.anim = new AniMaker(this.char, 4.35);

        //Add animations (JSON)
        //Generem les animacions dinamicament
        obj['cubeTronic'].geometry.animations.forEach((anim: THREE.AnimationClip, i: number) => {
            this.actions[anim.name] = new THREE.Animation(chara, anim, THREE.AnimationHandler.CATMULLROM);
            this.actions[anim.name].enabled = true;
        });
        
    }

    /**
     * Funcio de callback que s'executará un cop carregat el nivell
     * @memberof Game
     */
    public getLvl = (lvl: Level, cb: () => void): void => {
        this.level = lvl;
        this.generateLevel(cb);
    }

    /**
     * Funcio que genera la il·luminacio de escena
     * @memberof Game
     */
    generateScene(): void {
        this.clock = new THREE.Clock(false); //Ha de ser false o el clock seguirá contant tot i aturarlo (Bug THREEJS)
        let light = new THREE.AmbientLight( 0xd0d0d0 ); // llum d'ambient
        let pointLight = new THREE.PointLight( 0xffffff ); // punt de llum
        pointLight.position.x = 50;
        pointLight.position.y = 10;
        pointLight.position.z = -50;
        //pointLight.castShadow = true;
        this.scene.add( light );
        this.scene.add( pointLight );

        this.time = 0;
    }

    /**
     * Funcio que neteja tots els objectes de la escena.
     * @memberof Game
     */
    clearScene(): void {
        while(this.scene.children.length > 0){ 
            this.scene.remove(this.scene.children[0]); 
        }
    }

    /**
     * Funcio que activa la generacio del nivell
     * @memberof Game
     */
    generateLevel(cb: () => void) {
        if (this.level != undefined && this.objMaterials != undefined && Object.keys( this.objMaterials ).length > 0) {
            let obj: any = this.objMaterials;
            this.level.meshPlatforms = [[]];
            this.level.steppedPlatforms = [[]];
            this.generateScene();

            this.level.normalMaterials = Utils.genPlatformMaterials(this.level.defaultColor, 0.8);
            this.level.steppedMaterials = Utils.genPlatformMaterials(this.level.strColor, 0.8);
            let geo = new THREE.BoxGeometry( 2, 4, 2, 4, 4, 4 );

            //Creacio de plataformes
            for(let i = 0; i < this.level.platforms.length; i++) {
                this.level.meshPlatforms[i] = [];
                this.level.steppedPlatforms[i] = [];

                for(let j = 0; j < this.level.platforms[i].length; j++) {
                    if (this.level.platforms[i][j]) {
                        let cub = new THREE.Mesh( geo, new THREE.MeshFaceMaterial( this.level.normalMaterials ) );
                        this.level.meshPlatforms[i][j] = cub;
                        this.level.objPlatforms.push(cub);
                        this.level.steppedPlatforms[i][j] = false;

                        cub.position.x = 4.35 * j;
                        cub.position.y = -2;
                        cub.position.z = 4.35 * i;
                        //cub.receiveShadow = true;
                        //cub.castShadow = true;
                        this.scene.add(cub);
                    }
                }
            }

            //Generar icona de vides
            $( ".lives" ).remove();
            for (let i = 0; i < this.level.lives; i++) {
                $('#hud-lives').append(' <i class="sprite sprite-lives-small lives"></i>');
            }

            this.nPos = this.level.startPos;
            this.char.position.set(4.35 * this.level.startPos.z, 0.02, 4.35 * this.level.startPos.x - 0.65);
            this.scene.add(this.char);

            this.actions.idle.play();
            this.clock.start();

            cb(); //Callback que mostra el canvas i amaga menús

        }
    }
}